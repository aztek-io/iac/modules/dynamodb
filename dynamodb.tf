resource "aws_dynamodb_table" "this" {
  name           = local.name
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  hash_key       = var.hash_key["name"]

  attribute {
    name = var.hash_key["name"]
    type = var.hash_key["type"]
  }

  tags = local.tags
}

resource "aws_dynamodb_table_item" "this" {
  for_each   = toset(var.seed_data)
  table_name = aws_dynamodb_table.this.name
  hash_key   = aws_dynamodb_table.this.hash_key
  item       = each.value
}


