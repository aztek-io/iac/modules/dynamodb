# Module: dynamodb

Creates a dynampdb table in AWS.

## Example Usage

How to create a lambda function using this module:

```hcl
module "dynamodb" {
  source = "git@gitlab.com:aztek-io/iac/modules/dynamodb.git?ref=v0.1.0"
}
```


```hcl
module "other_dynamodb" {
  source   = "git@gitlab.com:aztek-io/iac/modules/dynamodb.git?ref=v0.1.0"
  name     = "${var.project}-${var.environment}-events"
  hash_key = {
    name = "event_id"
    type = "N"
  }
  seed_data = [
    <<-SEED
    {
      "event_id": {"N": "0"},
      "description": {"S": "Seed Data"}
    }
    SEED
    ,
    <<-OTHER_SEED
    {
      "event_id": {"N": "1"},
      "description": {"S": "Other Seed Data"}
    }
    OTHER_SEED
  ]
}
```
## Argument Reference

The following arguments are supported:

### Required Attributes

NONE

### Optional Attributes

* `hash_key`       - (Optional|map(string)) The attribute to use as the hash (partition) key. (Defaults to a map `{"name": "id", "type": "S"}`)
* `name`           - (Optional|string) Name of the repository. (Defaults to a random uuid via `local.name`)
* `read_capacity`  - (Optional|number) The number of read units for this table. (Defaults to `1`)
* `seed_data`      - (Optional|list(string)) a list of string to seed the table with. (Defaults to an empty list)
* `tags`           - (Optional|map(string)) A map of tags to assign to the object. (Defaults to a map with the Name tag always set via `local.tags`)
* `write_capacity` - (Optional|number) The number of write units for this table. (Defaults to `1`)

## Attribute Reference

* `name`    - Displays the repository name.
* `tags`    - Displays the resouce tags.
