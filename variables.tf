variable "hash_key" {
  default = {
    name = "id"
    type = "S"
  }
}

variable "name" {
  default = ""
}

variable "read_capacity" {
  default = 1
}

variable "seed_data" {
  type    = list(string)
  default = []
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "write_capacity" {
  default = 1
}
